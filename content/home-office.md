---
Title: Home-offices Planejados
Description: Home-offices Planejados
---
<section class="banner-interna banner-homeoffice">
	<div class="container title-banner text-center">
		<h2>Home-office</h2>
	</div>
</section>
<section class="content-wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>Se você tem o costume de trabalhar em casa, então precisa de um ambiente que te apoie e te deixe confortável. Confira abaixo algumas fotos.</p>
				<br>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="6"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="8"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="9"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="10"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="11"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="12"></li>
				  </ol>
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/1.jpg" alt="...">
					  <!--
				      <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/2.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				     <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/3.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 3</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/4.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				     <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/5.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 3</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/6.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				     <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/7.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 3</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/8.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				     <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/9.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 3</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/10.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				     <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/11.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 3</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/12.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				     <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/homeoffice/13.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 3</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>				
				  </div>
				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>		
	</div>
</section>
