---
Title: Cozinhas Planejadas
Description: Cozinhas Planejadas
---
<section class="banner-interna banner-cozinhas">
	<div class="container title-banner text-center">
		<h2>Cozinhas</h2>
	</div>
</section>
<section class="content-wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>A cozinha é um lugar onde passamos um grande tempo e por isso nós, da Augen, projetamos pensando em todo o conforto e facilidade para nossos clientes, sempre visando a qualidade dos materiais e a praticidade. Confira abaixo algumas fotos.</p>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="3"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="4"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="5"></li>
					<li data-target="#carousel-example-generic" data-slide-to="6"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="7"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="8"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="9"></li>
					<li data-target="#carousel-example-generic" data-slide-to="10"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="11"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="12"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="13"></li>
					<li data-target="#carousel-example-generic" data-slide-to="14"></li>
					<li data-target="#carousel-example-generic" data-slide-to="15"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="16"></li>
					<li data-target="#carousel-example-generic" data-slide-to="17"></li>
				  </ol>
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/1.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/2.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/3.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/4.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/5.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/6.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/7.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/8.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/9.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/10.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/11.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/12.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/13.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/14.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/15.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/16.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/17.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cozinhas/18.jpg" alt="...">
					  <!--  
					  <div class="carousel-caption">
				        <h3>Projeto 2</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>			
				  </div>
				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>		
	</div>
</section>
