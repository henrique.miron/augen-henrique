---
Title: Corporativo
Description: Corporativo
---
<section class="banner-interna">
	<div class="container title-banner text-center">
		<h2>CORPORATIVO</h2>
	</div>
</section>
<!--  
<section class="content-wraper">
	<h3 class="text-center">Conheça nossa equipe</h3>
	<div class="container">
		<div class="row">
			<div class="col-md-3 corp-content">
				<div class="img-corp">
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/silhueta.png" alt="...">
					<ul class="list-unstyled list-corp">
						<li><a href="#"><span class="ico-face fa fa-facebook ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-twitter fa fa-twitter ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-instagram fa fa-instagram ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-linkedin fa fa-linkedin ico-corp"></span></a></li>
					</ul>					
				</div>
				<h4 class="gray text-center">Fulano da Silva</h4>
				<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			<div class="col-md-3 corp-content">
				<div class="img-corp">
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/silhueta.png" alt="...">
					<ul class="list-unstyled list-corp">
						<li><a href="#"><span class="ico-face fa fa-facebook ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-twitter fa fa-twitter ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-instagram fa fa-instagram ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-linkedin fa fa-linkedin ico-corp"></span></a></li>
					</ul>					
				</div>
				<h4 class="gray text-center">Fulano da Silva</h4>
				<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			<div class="col-md-3 corp-content">
				<div class="img-corp">
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/silhueta.png" alt="...">
					<ul class="list-unstyled list-corp">
						<li><a href="#"><span class="ico-face fa fa-facebook ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-twitter fa fa-twitter ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-instagram fa fa-instagram ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-linkedin fa fa-linkedin ico-corp"></span></a></li>
					</ul>					
				</div>
				<h4 class="gray text-center">Fulano da Silva</h4>
				<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			<div class="col-md-3 corp-content">
				<div class="img-corp">
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/silhueta.png" alt="...">
					<ul class="list-unstyled list-corp">
						<li><a href="#"><span class="ico-face fa fa-facebook ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-twitter fa fa-twitter ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-instagram fa fa-instagram ico-corp"></span></a></li>
						<li><a href="#"><span class="ico-linkedin fa fa-linkedin ico-corp"></span></a></li>
					</ul>					
				</div>
				<h4 class="gray text-center">Fulano da Silva</h4>
				<p class="text-center">Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
		</div>
	</div>
</section>
-->
<section class="content-wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<h3>Processos</h3>
				<p>Desde a fabricação até a montagem. Todo nosso processo é regulamentado, garantindo a qualidade e a durabilidade de nossos projetos. Usamos materiais de primeira qualidade. Fabricamos cada item com cuidado e supervisão extremamente qualificada. Conheça mais sobre nossos projetos.</p>
				<h3>Equipe</h3>
				<p>Somente profissionais com altíssima qualidade, sempre monitorados e avalidos, garantindo a melhor mão de obra e total satisfação de nossos clientes.</p>
			</div>
			<div class="col-md-5">
				<img class="img-responsive" src="%base_url%/themes/augen/assets/img/corp-img.jpg" alt="...">
			</div>
		</div>
	</div>
</section>
