---
Title: Contato
Description: Contato
---
<section class="banner-interna">
	<div class="container title-banner text-center">
		<h2>ENTRE<br/>EM CONTATO</h2>
	</div>
</section>
<section class="content-wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h3 class="gray">ENVIE SUA PLANTA OU UMA MENSAGEM</h3>
				<div class="row">
					<div class="col-md-12">
						<form method="post" action="%base_url%/fp_mailer" class="contato-contato" enctype="multipart/form-data">
							<div class="form-group">
								<label for="exampleInputNome">Nome*</label>
								<input type="text" class="form-control" id="exampleInputNome"  placeholder="Nome" name="name" required>
							</div>
							<div class="form-group">
								<label for="exampleInputE-mail">E-mail*</label>
								<input type="email" class="form-control" id="exampleInputE-mail" placeholder="E-mail" name="email" required>
							</div>
							<div class="form-group">
								<label for="exampleInputTelefone">Telefone*</label>
								<input type="text" class="form-control" id="exampleInputTelefone" placeholder="Telefone" name="telefone" required>
							</div>
							<div class="form-group">
								<label for="exampleInputMensagem">Mensagem</label>
								<textarea class="form-control" rows="3" id="exampleInputMensagem" name="mensagem"></textarea>
							</div>
							<div class="form-group">
								<label for="exampleInputFile">Anexe sua planta aqui</label>
								<input type="file" id="exampleInputFile" name="planta">
							</div>
							<div class="form-group">
								<label for="exampleInputVisita">Agende sua visita! Indique a data abaixo</label>
								<input type="date" class="form-control" id="exampleInputVisita" placeholder="Data da visita" name="visita">
							</div>
							<button type="submit" class="btn btn-default">Enviar</button>
						</form>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<h3 class="gray">Ou através dos contatos abaixo:</h3>
				<address>
				  <strong>Telefones:</strong><br>
				  <i class="fa fa-phone icon"></i>11 2361.0607<br>
				</address>
				<address>
				  <strong>E-mail:</strong><br>
				  <i class="fa fa-envelope icon"></i><a href="mailto:augen.vendas@gmail.com">augen.vendas@gmail.com</a>
				</address>
				<address>
				  <strong>Endereço:</strong><br>
				  <i class="fa fa-map-marker icon"></i>Rua do Tatuapé, 196 - Tatuapé - São paulo, SP - CEP: 03089-030
				</address>
			</div>
		</div>
	</div>
</section>
<section>
	<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.0454859555794!2d-46.56541488443704!3d-23.530866366353287!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5efae0a1f4c9%3A0x7f4a4b75b08ae7cc!2sR.+do+Tatuap%C3%A9%2C+196+-+Maranhao%2C+S%C3%A3o+Paulo+-+SP%2C+03089-030!5e0!3m2!1spt-BR!2sbr!4v1457383186842" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
<script type="text/javascript" src="%theme_url%/assets/js/validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
$('.contato-contato').validate({
		rules: {	
			name: {		
				required: true	
				},		
			email: {
				required: true,		
				email:true		
				},
			telefone: {
				required: true
				}
			},messages: {
				name: 'Lembre-se de colocar o seu nome',	
				email: {	
					required: 'Por favor deixe insira seu email para podermos te dar uma resposta',
					email: 'Humm.. este email não me parece válido, poderia corrigi-lo por favor?'			
					},		
				telefone: 'Deixe seu telefone, deste modo é mais fácil te encontrarmos',
			},	submitHandler: function(form){		
					form.submit();	
				}	
		});
});
</script>