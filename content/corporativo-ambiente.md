---
Title: Ambientes Corporativos Planejados
Description: Ambientes Corporativos Planejados
---
<section class="banner-interna banner-corporativo">
	<div class="container title-banner text-center">
		<h2>Ambientes<br/>Corporativos</h2>
	</div>
</section>
<section class="content-wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<p>Sua empresa também merece um ambiente planejado, com uma impressão de maior organização e imponência. Confira abaixo algumas fotos.</p>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="1"></li>				    
				  </ol>
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="%base_url%/themes/augen/assets/img/corporativo/1.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/corporativo/2.jpg" alt="...">
					  <!--
					  <div class="carousel-caption">
				        <h3>Projeto 1</h3>
    					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
				      </div>
					  -->
				    </div>	
				  </div>
				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>		
	</div>
</section>
