---
Title: Móveis Planejados, Projetos Especiais e Sob Medida
Description: Qualidade, sofisticação e execução impecáveis em  móveis planejados, projetos especiais e sob medida
---
<section class="banner-home">
	<div class="container title-banner text-center">
		<h2>PROJETOS<br/>ESPECIAIS<br/>E SOB MEDIDA</h2>
		<p>Qualidade, sofisticação e execução impecáveis</p>
	</div>
</section>
<section class="banner-ambientes" data-parallax="scroll" data-image-src="%base_url%/themes/augen/assets/img/bg-ambientes.jpg">
	<div class="container">
		<div class="ambientes-content text-center">
			<h3 class="white">AMBIENTES</h3>
			<p class="white">Nada como receber os amigos em um ambiente agradável ou relaxar no seu quarto com todo o conforto e aconchego. Quarto, sala, cozinha, etc. Na Augen você encontra qualidade superior em todos os ambientes. Conheça nossos ambientes planejados. </p>
			<a href="%base_url%/ambientes" class="button-default-white" href="#">Ver ambientes planejados</a>
		</div>
	</div>
</section>
<section>
	<div id="owl-demo" class="owl-carousel owl-theme-home">
		<div class="item">
			<a class="item-link-1" href="quartos">+</a>
			<div class="item-link-2 text-left">
				<h4>Quarto de casal</h4>
				<p>Quarto de casal contemporâneo, com cama estilo futon, armários invisíveis e iluminação indireta, perfeita para uma noite romântica ou uma leitura leve.</p>
				<a class="button-default-gray" href="quartos">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto1.jpg" alt="">
		</div>
		<div class="item">
			<a class="item-link-1" href="salas">+</a>
			<div class="item-link-2 text-left">
				<h4>Sala de estar</h4>
				<p>Projeto com a assinatura Augen. Ambiente planejado com móveis brancos, com excelente iluminação natural e grande conforto. Acabemento em laca branco.</p>
				<a class="button-default-gray" href="salas">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto2.jpg" alt="">
		</div>
		<div class="item">
			<a class="item-link-1" href="cozinhas">+</a>
			<div class="item-link-2 text-left">
				<h4>Cozinha planejada</h4>
				<p>Cozinha planejada com detalhes modernos, excelente distribuição de espaços e ambiente confortável.</p>
				<a class="button-default-gray" href="cozinhas">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto3.jpg" alt="">
		</div>
		<div class="item">
			<a class="item-link-1" href="adegas">+</a>
			<div class="item-link-2 text-left">
				<h4>Adega</h4>
				<p>Adega desenvolvida para criar uma maior facilidade na temperatura ambiente das garrafas de vidro.</p>
				<a class="button-default-gray" href="adegas">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto4.jpg" alt="">
		</div>
		<div class="item">
			<a class="item-link-1" href="banheiros">+</a>
			<div class="item-link-2 text-left">
				<h4>Banheiro luxuoso</h4>
				<p>Projeto criado para o maior conforto e relaxamento, ambientação com vidros temperados e banheira.</p>
				<a class="button-default-gray" href="banheiros">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto5.jpg" alt="">
		</div>
		<div class="item">
			<a class="item-link-1" href="closet">+</a>
			<div class="item-link-2 text-left">
				<h4>Closet</h4>
				<p>Closet desenvolvido para casal, com prioridade nos espaços compactos e bem organizados.</p>
				<a class="button-default-gray" href="closet">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto6.jpg" alt="">
		</div>
		<div class="item">
			<a class="item-link-1" href="corporativo-ambiente">+</a>
			<div class="item-link-2 text-left">
				<h4>Corporativo</h4>
				<p>Projetado para dar um ambiente se sofisticação e seriedade empresarial.</p>
				<a class="button-default-gray" href="corporativo-ambiente">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto7.jpg" alt="">
		</div>
		<div class="item">
			<a class="item-link-1" href="home-office">+</a>
			<div class="item-link-2 text-left">
				<h4>Home office</h4>
				<p>Ambiente projetado com "ar" de escritório, para facilitar a produtividade do cliente trabalhando em casa.</p>
				<a class="button-default-gray" href="home-office">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto8.jpg" alt="">
		</div>
		<div class="item">
			<a class="item-link-1" href="lavanderia">+</a>
			<div class="item-link-2 text-left">
				<h4>Lavanderia</h4>
				<p>Mais uma assinatura de gala da Augen. Esse projeto foi categorizado pela distribuição excelente de cada espaço da lavenderia.</p>
				<a class="button-default-gray" href="lavanderia">Mais...</a>
			</div>
			<img class="img-responsive" src="%base_url%/themes/augen/assets/img/projeto9.jpg" alt="">
		</div>
	</div>
</section>
  
<section class="section-testimonials">
	<div class="container">
		<div class="row text-center">
			<div class="col-sm-8 col-sm-offset-2">
				<h3 class="gray">Depoimentos</h3>
				<p>Temos tanta confiança nos nossos produtos e serviços que nossos depoimentos são gravados pelos próprios clientes, sem estúdios e grandes produções. Assista e comprove.</p>
			</div>
		</div>
			<div class="row text-center">
			 <div class="col-md-10 col-md-offset-1">
				<div class="col-md-4 text-center">
					<div class="depoiment-circle circle1"></div>
					<div class="depoiment-name">
						<p><strong>Cristiano e Renata</strong></p>
						<span><i>"Os móveis da Augen encantam pela qualidade do material, design, sofisticação e acabamento. Equipe totalmente preparada, desde o pré projeto até o último detalhe. Os prazos de entrega, de início e término da montagem são respeitados de acordo com o contrato. Com certeza, superaram as nossas expectativas."</i></span>
					</div>
					<div class="depoiment-links">
						<ul class="list-unstyled">
							<!-- <li><a href="#" data-toggle="modal" data-target=".modal-testimonials"><span class="fa fa-caret-right"></span>Assista ao depoimento</a></li> -->
							<li><a href="#" data-toggle="modal" data-target=".modal-testimonials1">Conheça o projeto</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 text-center">
					<div class="depoiment-circle circle2"></div>
					<div class="depoiment-name">
						<p><strong>Karen</strong></p>
						<span><i>"Empresa cumpre oque promete,total assesoria na criação do projeto,organizada,entregou e finalizou a obra antes do prazo de contrato,mão de obra excelente de alto padrão,só tenho a elogiar e parabenizar todos da empresa augen,pois meu apartamento ficou maravilhoso!!!"</i></span>
					</div>
					<div class="depoiment-links">
						<ul class="list-unstyled">
							<!-- <li><a href="#" data-toggle="modal" data-target=".modal-testimonials"><span class="fa fa-caret-right"></span>Assista ao depoimento</a></li> -->
							<li><a href="#" data-toggle="modal" data-target=".modal-testimonials2">Conheça o projeto</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-4 text-center">
					<div class="depoiment-circle">
						Valéria
					</div>
					<div class="depoiment-name">
						<p><strong>Valéria</strong></p>
						<span><i>"Eu sou a Valéria de São Caetano do Sul. Gostaria de dar meu depoimento para manifestar minha satisfação nos serviços prestados pela Augen Planejados. Eu e minha família fomos super bem atendidos pelo Rafael, que prontamente fez um projeto impecável obedecendo nossas vontades..."</i></span>
					</div>
					<div class="depoiment-links">
						<ul class="list-unstyled">
							<!-- <li><a href="#" data-toggle="modal" data-target=".modal-testimonials"><span class="fa fa-caret-right"></span>Assista ao depoimento</a></li> -->
							<li><a href="#" data-toggle="modal" data-target=".modal-testimonials3">Conheça o projeto</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Modais de Depoimentos -->
<div class="modal fade modal-testimonials1" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title" id="gridSystemModalLabel">Cristiano e Renata</h3>
			</div>
			<div class="modal-body">
				<p><i>"Os móveis da Augen encantam pela qualidade do material, design, sofisticação e acabamento. Equipe totalmente preparada, desde o pré projeto até o último detalhe. Os prazos de entrega, de início e término da montagem são respeitados de acordo com o contrato. Com certeza, superaram as nossas expectativas."</i></p>
				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
				  </ol>
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="%base_url%/themes/augen/assets/img/cristiano-renata/imagem1.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/cristiano-renata/imagem2.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				  </div>
				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade modal-testimonials2" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title" id="gridSystemModalLabel">Karen</h3>
			</div>
			<div class="modal-body">
				<p><i>"Empresa cumpre oque promete,total assesoria na criação do projeto,organizada,entregou e finalizou a obra antes do prazo de contrato,mão de obra excelente de alto padrão,só tenho a elogiar e parabenizar todos da empresa augen,pois meu apartamento ficou maravilhoso!!!"</i></p>
				<div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic2" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic2" data-slide-to="1"></li>
				    <li data-target="#carousel-example-generic2" data-slide-to="2"></li>
				    <li data-target="#carousel-example-generic2" data-slide-to="3"></li>
				  </ol>
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="%base_url%/themes/augen/assets/img/karen/imagem1.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/karen/imagem2.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/karen/imagem3.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/karen/imagem4.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				  </div>
				  <!-- Controls -->
				  <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="modal fade modal-testimonials3" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h3 class="modal-title" id="gridSystemModalLabel">Valéria</h3>
			</div>
			<div class="modal-body">
				<p><i>"Eu sou a Valéria de São Caetano do Sul. Gostaria de dar meu depoimento para manifestar minha satisfação nos serviços prestados pela Augen Planejados. Eu e minha família fomos super bem atendidos pelo Rafael, que prontamente fez um projeto impecável obedecendo nossas vontades. Após a primeira visita em poucos dias já nos apresentou o projeto dos móveis que na sequência foi encaminhado a fábrica para corte e execução e a entrega se deu 10 dias após o pedido chegar na fábrica. Todo o processo desde a sua primeira visita até a instalação de todos os móveis se deu em 20 dias e ficou tudo maravilhoso como exatamente esperávamos e desejávamos. Obrigada a Augen e ao Rafael que proporcionaram a realização de um sonho sem nenhuma dor de cabeça, e ainda com um super atendimento pós venda. Abraços"</i></p>
				<!-- <div id="carousel-example-generic2" class="carousel slide" data-ride="carousel">
				  <!-- Indicators 
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic2" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic2" data-slide-to="1"></li>
				    <li data-target="#carousel-example-generic2" data-slide-to="2"></li>
				    <li data-target="#carousel-example-generic2" data-slide-to="3"></li>
				  </ol>
				  <!-- Wrapper for slides 
				  <div class="carousel-inner" role="listbox">
				    <div class="item active">
				      <img src="%base_url%/themes/augen/assets/img/karen/imagem1.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/karen/imagem2.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/karen/imagem3.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				    <div class="item">
				      <img src="%base_url%/themes/augen/assets/img/karen/imagem4.jpg" alt="...">
				      <div class="carousel-caption">
				        Cozinha Planejada
				      </div>
				    </div>
				  </div>
				  <!-- Controls 
				  <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				    <span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				    <span class="sr-only">Next</span>
				  </a>
				</div> -->
			</div>
		</div>
	</div>
</div>
<!-- Modais de Depoimentos -->
<section class="contato-home">
	<div class="container">
		<h3 class="white text-center">ENVIE SUA PLANTA OU UMA MENSAGEM</h3>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<form method="post" action="%base_url%/fp_mailer" class="form-contato-home" enctype="multipart/form-data">
					<div class="form-group">
						<label for="exampleInputNome">Nome*</label>
						<input type="text" name="name" class="form-control" id="exampleInputNome" placeholder="Nome">
					</div>
					<div class="form-group">
						<label for="exampleInputE-mail">E-mail*</label>
						<input type="email" name="email" class="form-control" id="exampleInputE-mail" placeholder="E-mail">
					</div>
					<div class="form-group">
						<label for="exampleInputTelefone">Telefone*</label>
						<input type="text" name="telefone" class="form-control" id="exampleInputTelefone" placeholder="Telefone">
					</div>
					<div class="form-group">
						<label for="exampleInputTelefone">Mensagem*</label>
						<textarea name="mensagem" class="form-control" rows="3"></textarea>
					</div>
					<div class="form-group">
						<label for="exampleInputFile">Anexe sua planta aqui.</label>
						<input type="file" id="exampleInputFile" name="planta">
					</div>
					<button type="submit" class="btn btn-default">Enviar</button>
				</form>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript" src="%theme_url%/assets/js/validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){	
$('.form-contato-home').validate({
		rules: {	
			name: {		
				required: true	
				},		
			email: {
				required: true,		
				email:true		
				},
			telefone: {
				required: true
				},
			mensagem: {
				required: true
				},	
			},messages: {
				name: 'Lembre-se de colocar o seu nome',	
				email: {	
					required: 'Por favor deixe insira seu email para podermos te dar uma resposta',
					email: 'Humm.. este email não me parece válido, poderia corrigi-lo por favor?'			
					},		
				telefone: 'Deixe seu telefone, deste modo é mais fácil te encontrarmos',
				mensagem: 'Não se esqueça de preencher a mensagem',
			},	submitHandler: function(form){		
					form.submit();	
				}	
		});		
});
</script>