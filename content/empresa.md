---
Title: Empresa
Description: Empresa
---
<section class="banner-interna">
	<div class="container title-banner text-center">
		<h2>SOBRE<br/>A AUGEN</h2>
	</div>
</section>
<section class="content-wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-push-6">
				<br>
				<br>
				<br>
				<h3 class="gray">Conheça melhor a Augen</h3>
				<p>AUGEN móveis sob medida uma empresa com 8 anos de experiência,criada pela necessidade dos profissionais dos ramos de arquitetura,engenharia e decoração em oferecer um produto de qualidade impecável com melhor custo beneficio do mercado,criamos móveis pensando na necessidade do nosso cliente,somos uma empresa muito criteriosa desde a medição,criação e execução do projeto.a maioria dos nossos clientes são indicações feitas por nossos proprios clientes que conseguimos ao longo dos anos oferecendo um produto com materia prima de alto padrão,mão de obra qualificada,fazendo com que a empresa seja reconhecida pela sua seriedade e respeito pelos seus clientes.</p>
			</div>
			<div class="col-md-6 col-md-pull-6">
				<img class="img-responsive" src="%base_url%/themes/augen/assets/img/img-interna.jpg" alt="">
			</div>
		</div>
	</div>
</section>
<section class="content-carousel" data-parallax="scroll" data-image-src="%base_url%/themes/augen/assets/img/bg-contato.jpg">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<div id="owl-content" class="owl-carousel owl-theme text-center">
					<div class="item">
						<h3 class="white">Missão</h3>
						<p>Surgimos com a missão de entregar soluções inovadoras com qualidade. Tentando sempre atender da melhor forma quem busca por ambientes aconchegantes e e modernos.</p>
					</div>
					<div class="item">
						<h3 class="white">Visão</h3>
						<p>Pretendemos nos tornar referência no mercado de móveis planejados.</p>
					</div>
					<div class="item">
						<h3 class="white">Valores</h3>
						<p>A Augen tem como seus principais valores a inovação, qualidade, preços acessíveis, compromisso com o meio ambiente e a satisfação total de seus clientes.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
