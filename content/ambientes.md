---
Title: Ambientes Planejados
Description: Ambientes Planejados
---
<section class="banner-interna">
	<div class="container title-banner text-center">
		<h2>AMBIENTES</h2>
	</div>
</section>
<section class="content-wraper">
	<div class="container">
		<h3 class="text-center">Projetos</h3>
		<p>Confira um pouco sobre nossos projetos e conheça alguns ambientes projetados e executados por nós da Augen. Contamos com projetos de quartos, cozinhas, salas e muito mais! Veja abaixo.</p>
		<br>
		<div class="row">
			<div class="col-md-4">
				<div class="item">
					<a href="quartos" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="quartos" class="button-default-gray">Quartos planejados</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/quartos/1.jpg" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<a href="salas" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="salas" class="button-default-gray">Salas sob medida</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/salas/1.jpg" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<a href="cozinhas" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="cozinhas" class="button-default-gray">Cozinhas planejadas</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/cozinhas/1.jpg" alt="">
				</div>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-4">
				<div class="item">
					<a href="adegas" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="adegas" class="button-default-gray">Adegas</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/adegas/1.jpg" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<a href="banheiros" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="banheiros" class="button-default-gray">Banheiro</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/banheiro/1.jpg" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<a href="closet" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="closet" class="button-default-gray">Closet</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/closet/1.jpg" alt="">
				</div>
			</div>
		</div>
		<br/>	
		<div class="row">
			<div class="col-md-4">
				<div class="item">
					<a href="corporativo-ambiente" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="corporativo-ambiente" class="button-default-gray">Corporativo</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/corporativo/1.jpg" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<a href="home-office" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="home-office" class="button-default-gray">Home office</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/homeoffice/1.jpg" alt="">
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<a href="lavanderia" class="item-link-1">+</a>
					<div class="item-link-2">
						<br>
						<br>
						<br>
						<a href="lavanderia" class="button-default-gray">Lavanderia</a>
					</div>
					<img class="img-responsive" src="%base_url%/themes/augen/assets/img/lavanderia/1.jpg" alt="">
				</div>
			</div>
		</div>	
	</div>
</section>
