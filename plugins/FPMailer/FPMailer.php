﻿<?php

/**
 * 4Pixels Mailer
 *
 * Send authenticated e-mail through an SMTP server
 *
 * @author  4Pixels Agência Digital
 * @link    http://ad4pixels.com.br
 * @license http://opensource.org/licenses/MIT
 * @version 1.0
 */
final class FPMailer extends AbstractPicoPlugin
{
    /**
     * This plugin is enabled by default?
     *
     * @see AbstractPicoPlugin::$enabled
     * @var boolean
     */
    protected $enabled = true;

    /**
     * Custom attributes
     */
    protected $config;
    protected $mailer;

    protected $url = 'fp_mailer';

    protected $smtp_config = array(
        'host'     => 'smtpout.secureserver.net',
        'user'     => 'augenmoveis@augenmoveis.com.br',
        'password' => 'augen123moveis',
        'port'     => '465',
		'secure'   => 'tls'
    );

    protected $back_office_config = array(
    	'to'      => 'augen.vendas@gmail.com',
    	'name'    => 'Site Augen Móveis',
    	'subject' => 'Contato via website',
    );

    protected $user_config = array(
    	'subject' => 'Obrigado'
    );

    /**
     * Triggered after Pico has read its configuration
     *
     * @see    Pico::getConfig()
     * @param  mixed[] &$config array of config variables
     * @return void
     */
    public function onConfigLoaded(array &$config)
    {
        $this->config             = $config;
        $this->smtp_config        = (object) $this->smtp_config;
        $this->back_office_config = (object) $this->back_office_config;
		$this->user_config        = (object) $this->user_config;
    }

    /**
     * Triggered after Pico has evaluated the request URL
     *
     * @see    Pico::getRequestUrl()
     * @param  string &$url part of the URL describing the requested contents
     * @return void
     */
    public function onRequestUrl(&$url)
    {
        $this->enabled = (bool) ( $this->url == $url );
    }

    /**
     * Triggered after Pico has read the contents of the file to serve
     *
     * @see    Pico::getRawContent()
     * @param  string &$rawContent raw file contents
     * @return void
     */
    public function onContentLoaded(&$rawContent)
    {
        if ( $this->enabled !== true )
            return;

        include_once __DIR__ . '/class.phpmailer.php';
        include_once __DIR__ . '/class.smtp.php';

		$this->mailer = new PHPMailer;

		//$this->mailer->SMTPDebug = 2;
		$this->mailer->CharSet = 'UTF-8';

		//$this->mailer->isSMTP();
		$this->mailer->SMTPAuth   = true;
		$this->mailer->CharSet	  = 'UTF-8';
		$this->mailer->Host       = $this->smtp_config->host;
		$this->mailer->SMTPSecure = $this->smtp_config->secure;
		$this->mailer->Username   = $this->smtp_config->user;
		$this->mailer->Password   = $this->smtp_config->password;
		$this->mailer->Port       = $this->smtp_config->port;

		$this->mailer->isHTML(true);

		$json = array();

		//$json['user'] = $this->send_user();
		$json['back_office'] = $this->send_back_office();

		header("Location: {$this->config['base_url']}obrigado");
	}

	public function send_back_office()
	{
		$this->mailer->ClearAddresses();

		// User information
		$this->mailer->From = $_POST['email'];
		$this->mailer->FromName = $_POST['name'];
		$this->mailer->addReplyTo($_POST['email']);

		// Company information
		$this->mailer->addAddress($this->back_office_config->to);

		// Email information
		$date = date('H:i d/m/Y');

		$this->mailer->Subject = $this->back_office_config->subject;

		$this->mailer->Body    = <<<HTML
		<h2>Contato via site</h2>
		<br>
		<p><strong>Nome: </strong> {$_POST['name']}</p>
		<p><strong>E-mail: </strong> {$_POST['email']}</p>
		<p><strong>Telefone: </strong> {$_POST['telefone']}</p>
		<p><strong>Mensagem: </strong> {$_POST['mensagem']}</p>
HTML;

        if (isset($_POST['visita']))
            $this->mailer->Body .= "<p><strong>Data da visita: </strong> {$_POST['visita']}</p>";

        if ($_FILES['planta']['error'] == 0) {
            $path = '../../uploads/';
            $ext  = strrpos($_FILES['planta']['name'], '.');
            $pos  = strlen($_FILES['planta']['name']) - $ext;
            $file = time() . substr($_FILES['planta']['name'], $pos * -1, $pos);
			
            move_uploaded_file($_FILES['planta']['tmp_name'], 'uploads/'.$file);
			
            $url = $this->config['base_url'] . '/uploads/' . $file;
	
            $this->mailer->Body .= "<p><strong>Planta: </strong> <a href='{$url}'>ver arquivo</a></p>";
        }
		
		// if($this->mailer->send())
		// {
			
		// 	echo 'deu certo';
		// }	
		// else
		// {
		// 	 echo 'Not sent: <pre>'.print_r(error_get_last(), true).'</pre>';

			
		// }	
		// die;
		return $this->mailer->send();
		
	}

	public function send_user()
	{
		$this->mailer->ClearAddresses();

		// Company information
		$this->mailer->From = $this->back_office_config->to;
		$this->mailer->FromName = $this->back_office_config->name;
		$this->mailer->addReplyTo($this->back_office_config->to, $this->back_office_config->name);

		// User information
		$this->mailer->addAddress($_POST['email']);

		// Email information
		$date = date('H:i d/m/Y');

		$this->mailer->Subject = $this->user_config->subject;

		$this->mailer->Body    = <<<HTML
		<h2>Obrigado por entrar em contato!</h2>
		<br>
		<p>Olá {$_POST['name']}, recebemos o seu contato e retornaremos assim que possível.</p>
		<br>
		<p>Obrigado, <br>
		Augen Móveis Sob Medida</p>
		<br>
		<p>Enviado em {$date}</p>
HTML;

		$this->mailer->AltBody = <<<TXT
		Obrigado por entrar em contato! \n
		=============================== \n
		\n
		Olá {$_POST['name']}, recebemos o seu contato e retornaremos assim que possível. \n
		\n
        Obrigado, \n
		Augen Móveis Sob Medida \n
		\n
		Enviado em {$date}
TXT;

		//return $this->mailer->send();
	}
}
