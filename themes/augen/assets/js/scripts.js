$(document).ready(function() {

  var owl = $("#owl-demo");

  owl.owlCarousel({

      itemsCustom : [
        [0, 1],
        [450, 2],
        [600, 3],
        [700, 3],
        [1000, 4],
        [1200, 5],
        [1400, 6],
        [1600, 8]
      ],
      navigation : true,
      navigationText: [ '<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>' ]

  });

});


$(document).ready(function() {

  $("#owl-content").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem:true,

      navigationText: [ '<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>' ]

      // "singleItem:true" is a shortcut for:
      // items : 1,
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false

  });

});
